# LOKI Image Processing and Classification

This project contains R-code created by Dr. Jan Schulz and Claudia Thölen, (ICBM, Uni Oldenburg)
The LOKI-Complete-data-primer was improved within the master thesis of C.Thölen.
The additional R-code is used for data preparation, and classification.