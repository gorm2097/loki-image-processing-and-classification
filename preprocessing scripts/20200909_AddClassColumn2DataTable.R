# AddClassColumn2DataTable.R

# Claudia Th�len, ICBM, Uni Oldenburg

# created:       August 2020
# last changes:  09.09.2020


# This script takes the .primedIMGdata table from the LOKI-Complete-data-primer.R and adds a column 
# with the Class name.
# This only works if the primed images where sorted into folders with their class name.
# The class name is extracted from the filepath, which is included within the .primedIMGdata table.
# This script works as a data preparation to split the dataset into training and testing sets for
# classification purposes. The next script to use is the CreateTrainAndTestDataset_FromClassedTable.R
#--------------------------------------------------------------------------------------------------------------

# define paths for the .primedIMGdata file and the .primedIMGdata file name
file_path             = "C:/Users/cltho/OneDrive/Masterarbeit/Program/Classification/data_Sognefjord37057"
file_name             = "ClassedSognefjord37057.primedIMGdata"

# set working directory
setwd (file_path)

data = read.delim(file_name)

# create a column to attach to the table
c = matrix(data = NA, nrow = nrow(data), ncol = 1)

# attach the new coloum and name it Category
datac = cbind(data,c)
colnames(datac)[114] = "Category"

# the vector classes needs to contain all class names in order to find the respective data in the table
classes_14 = c("Polychaeta",
               "Bubble",
               "Chaetognatha",
               "Cnidaria",
               "Copepoda",
               "Ctenophora",
               "Detritus",
               "Egg",
               "Faeces",
               "Amphipoda",
               "Euphausiidae",
               "Mysidae",
               "Ostracoda",
               "Acantharia")



for (i in 1:nrow(datac)){
  # this loop iterates through the entire data table and extracts the folder name with the class information
  split = strsplit(datac[i,11], "/")  # 11 Col File Relativ Path
  
  folder_name = split[[1]][1]
  
  # Here the folder is labled "Class - Familiy", so the single words are split up
  list_class = strsplit(folder_name, " - ")[[1]]
  
  for (j in 1:length(classes_14)){
    
    # go through the class vector and search for the entry that matches the folder name
    if (is.element(classes_14[j], list_class)){
      
      # definde the new class name by using the class vectors entry
      class_name = classes_14[j]
      
    }
    
  }
  
  # set the class name
  datac[i,114] = class_name
  
  if (i %% 100 == 0){# print status of loop
    print(i)
  }
  
}

# there is no second space sign in the folder name for faeces so it was changed manually
datac[36865:37057, 114] = "Faeces"
# move class name one column up
datac[113] = datac[114]
datac = datac[ ,1:113]


# save the new table with the added class name in a new .primedIMGdata table
write.table (datac,
             file = "ClassedSognefjord37057_14classes.primedIMGdata",
             sep = "\t",
             row.names = FALSE,
             col.names = TRUE
)
