%% LOKInet for LOKI plankton image classification using a transfer learning AlexNet CNN
% Claudia Th�len, ICBM, Uni Oldenburg
% Created: August 2020
% last changed: 09.09.2020

% uses toolboxes:
% Deep Learning Toolbox
% Deep Learning Toolbox Model for AlexNet Network
%% clear history, figures and console
close all 
clear
clc
%% Load the alexnet architecture an the datapath into the MATLAB workspace

datapath = 'E:/Sognefjord_General_Augmented';

net = alexnet;

%% Build Datastore, Label and Split dataset
% create datastore, include all subfolders and use their names as labels
plds = imageDatastore(datapath,'IncludeSubfolders',true,'LabelSource','foldernames');

%create training and testing dataset
[plTrain, plVal, plTest] = splitEachLabel(plds, 190, 30,'randomized');

% get the number classes to adjust net accordingly
numClasses = length(unique(plTrain.Labels));

%% label and resize for alexnet
% CNNs work with a fixed size of input images
% here its 227 x 227 x 3, hence the gray to RGB convertion

AplTrain = augmentedImageDatastore([227 227],plTrain,'ColorPreprocessing','gray2rgb');

AplTest = augmentedImageDatastore([227 227],plTest,'ColorPreprocessing','gray2rgb');

AplVal = augmentedImageDatastore([227 227],plVal,'ColorPreprocessing','gray2rgb');

%% Adapt the alexnet architecture to fit the dataset and class size
% Create a new fully connected layer called fc with numClasses neurons 
% for the number of plankton classes

layers = net.Layers;

fc = fullyConnectedLayer(numClasses);

% replace the fully connected layer for the classes and the output layer
% with new layers
layers(23) = fc;
layers(end) = classificationLayer;


%% Set training options
% The learning rate controls how aggressively the algorithm changes the 
% network weights. The goal of transfer learning is to fine-tune an 
% existing network, so you typically want to change the weights less 
% aggressively than when training from scratch.

% 0.001 learning rate
% 0.9 momentum 
% 2 val freq
% = 87.75 % success

options = trainingOptions('sgdm','InitialLearnRate', 0.01,...
    'Momentum',0.9,...
    'ValidationData',AplVal,...
    'ValidationFrequency',2,...
    'Plots','training-progress');

%% Train network --> LOKInet

[LOKInet, info] = trainNetwork(AplTrain, layers, options)
save("temp_LOKInet.mat", "LOKInet")

%% Classify the test set
tic
plPreds = classify(LOKInet, AplTest);
save("temp_plPreds.mat", "plPreds")
toc
%% Evaluate the classification accuracy

% Store actual Labels of Test set
plActual = plTest.Labels;

% Check how many were classified correctly
numCorrect = nnz(plPreds == plActual)
% Fraction
fracCorrect = numCorrect/numel(plPreds) * 100

% Confusion matrix
cm = confusionchart(plActual,plPreds)

% best confusion matrix settings for 14 classes to get a nice image out 
% of the figure
cm.ColumnSummary = 'column-normalized';
cm.RowSummary = 'absolute';
cm.FontSize = 18;
cm.InnerPosition = [0.1,0.145,0.89,0.85];
cm.Position = [0.1,0.145,0.89,0.85];